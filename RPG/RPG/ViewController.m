//
//  ViewController.m
//  RPG
//
//  Created by Allison Lindner on 11/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "ViewController.h"
#import "VCGameWelcome.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [self animateHomeView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"buttonJogar"]) {
        VCGameWelcome *welcomeVC = [segue destinationViewController];
        welcomeVC.characterName = _textNameCharacter.text;
    }
}

- (void)animateHomeView {
    //Animation Title Label
    [_labelTitle setAlpha:0.0];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDelay:1.0];
    [UIView setAnimationDuration:1.5];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    [_labelTitle setAlpha:1.0];
    [UIView commitAnimations];
    //Fim Animation Title Label
    
    //Animation Button Jogar
    [_buttonJogar setFrame: CGRectMake(_myView.frame.size.width, _myView.frame.size.height + 600, _buttonJogar.frame.size.width,  _buttonJogar.frame.size.height)];
    [_buttonJogar setAlpha:0.0];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDelay:1.5];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    [_buttonJogar setAlpha:1.0];
    [_buttonJogar setFrame: CGRectMake(_myView.frame.size.width, _myView.frame.size.height, _buttonJogar.frame.size.width,  _buttonJogar.frame.size.height)];
    [UIView commitAnimations];
    //Fim Animation Button Jogar
    
    //Animation Button Créditos
    [_buttonCreditos setFrame: CGRectMake(_myView.frame.size.width - 600, _myView.frame.size.height, _buttonCreditos.frame.size.width,  _buttonCreditos.frame.size.height)];
    [_buttonCreditos setAlpha:0.0];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDelay:2.0];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    
    [_buttonCreditos setFrame: CGRectMake(_myView.frame.size.width, _myView.frame.size.height, _buttonCreditos.frame.size.width,  _buttonCreditos.frame.size.height)];
    [_buttonCreditos setAlpha:1.0];
    [UIView commitAnimations];
    //Fim Animation Button Créditos
    
    //Animation Button Info
    [_buttonInfo setFrame: CGRectMake(_myView.frame.size.width + 600, _myView.frame.size.height, _buttonInfo.frame.size.width,  _buttonInfo.frame.size.height)];
    [_buttonInfo setAlpha:0.0];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDelay:2.0];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    
    [_buttonInfo setFrame: CGRectMake(_myView.frame.size.width, _myView.frame.size.height, _buttonInfo.frame.size.width,  _buttonInfo.frame.size.height)];
    [_buttonInfo setAlpha:1.0];
    [UIView commitAnimations];
    //Fim Animation Button Info
    
    //Animation Text Field Name Character
    [_textNameCharacter setAlpha:0.0];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDelay:3.0];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    [_textNameCharacter setAlpha:1.0];
    [UIView commitAnimations];
    //Fim Animation Text Field Name Character
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if(textField) {
        [textField resignFirstResponder];
    }
    
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if(textField == self.textNameCharacter){
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 100), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    if(textField == self.textNameCharacter){
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 100), self.view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
    }
}

@end
