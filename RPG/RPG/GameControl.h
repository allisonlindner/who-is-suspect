//
//  GameControl.h
//  RPG
//
//  Created by Allison Lindner on 14/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameControl : NSObject

@property int failAcusations;
@property int successAcusations;
@property BOOL success;

@end
