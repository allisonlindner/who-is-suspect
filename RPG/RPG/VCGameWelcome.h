//
//  VCGameWelcome.h
//  RPG
//
//  Created by Allison Lindner on 12/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCGameWelcome : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelCharName;
@property (weak, nonatomic) IBOutlet UILabel *labelMissionNotifier;
@property (weak, nonatomic) IBOutlet UIButton *buttonGoMission;
@property (strong, nonatomic) IBOutlet UIView *myView;
@property (weak, nonatomic) IBOutlet UITextView *heroInfo;
@property (weak, nonatomic) IBOutlet UIButton *buttonGo;
@property (weak, nonatomic) IBOutlet UIButton *buttonHome;

@property NSString *characterName;

@property BOOL ready;
@property BOOL firstRun;

- (IBAction) closeView:(id)sender;
- (IBAction) buttonGoAndBackPressed:(id)sender;
- (void) animateView;

@end
