//
//  VCCrimeScene.h
//  RPG
//
//  Created by Allison Lindner on 12/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CrimeScene.h"

@interface VCCrimeScene : UIViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *buttonInfoVictim;
@property (weak, nonatomic) IBOutlet UIButton *buttonSuspects;
@property (weak, nonatomic) IBOutlet UIButton *buttonNextScene;
@property (weak, nonatomic) IBOutlet UIButton *buttonBackScene;
@property (strong, nonatomic) IBOutlet UIView *myView;
@property (weak, nonatomic) IBOutlet UILabel *labelAnalizeTitle;
@property (weak, nonatomic) IBOutlet UITextView *textAnalize;

- (IBAction) closeView:(id)sender;

- (void) animateView;
- (void) verifyAcusation;

- (IBAction) nextSceneRoom:(id)sender;
- (IBAction) backSceneRoom:(id)sender;

- (void) showInformationScene;

@end