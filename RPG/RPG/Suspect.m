//
//  Suspect.m
//  RPG
//
//  Created by Allison Lindner on 14/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "Suspect.h"

@implementation Suspect

- (void) loadDataWithArray:(NSArray *)data {
    
    _name = data[0];
    _details = data[1];
    _destiny = data[2];
    
}

@end
