//
//  CrimeScene.h
//  RPG
//
//  Created by Allison Lindner on 13/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface CrimeScene : NSObject

@property Person *victim;
@property NSMutableArray *suspects;
@property NSMutableArray *analizeTitle;
@property NSMutableArray *analizeText;

@property int sizeAnalize;
@property int sizeSuspects;

@property NSString* currentScene;
@property int currentSceneInt;
@property int currentSceneRoom;
@property int acuseResult;
@property int numberOfCases;

- (void) LoadDataWithSceneNumber:(NSString*)sceneNumber;

@end
