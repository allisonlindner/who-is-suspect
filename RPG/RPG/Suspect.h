//
//  Suspect.h
//  RPG
//
//  Created by Allison Lindner on 14/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Suspect : NSObject

@property NSString* name;
@property NSString* details;
@property NSString* destiny;

- (void) loadDataWithArray:(NSArray*)data;

@end
