//
//  CrimeScene.m
//  RPG
//
//  Created by Allison Lindner on 13/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "CrimeScene.h"
#import "FileManager.h"
#import "Person.h"
#import "Suspect.h"

@implementation CrimeScene

- (void) LoadDataWithSceneNumber:(NSString*)sceneNumber {
    
    NSString *victimFilePath;
    NSString *sceneFilePath;
    NSString *suspectFilePath;
    
    victimFilePath = [@"victim_" stringByAppendingString: sceneNumber];
    victimFilePath = [victimFilePath stringByAppendingString:@".data"];
    
    sceneFilePath = [@"scene_info_" stringByAppendingString: sceneNumber];
    sceneFilePath = [sceneFilePath stringByAppendingString:@".data"];
    
    suspectFilePath = [@"suspects_" stringByAppendingString: sceneNumber];
    suspectFilePath = [suspectFilePath stringByAppendingString:@".data"];
    
    NSArray *data;
    data = [[FileManager LoadStringFromFile:victimFilePath useBundle:true] componentsSeparatedByString:@";"];
    
    _victim = [Person new];
    
    _victim.name = data[0];
    _victim.age = data[1];
    _victim.state = data[2];
    _victim.details = data[3];
    
    NSArray *dataAnalizes;
    
    dataAnalizes = [[FileManager LoadStringFromFile:sceneFilePath useBundle:true] componentsSeparatedByString:@"|"];
    
    _analizeText = [[NSMutableArray alloc]init];
    _analizeTitle = [[NSMutableArray alloc]init];
    
    _sizeAnalize = (int)[dataAnalizes[0] integerValue];
    int i = 1;
    while (i <= _sizeAnalize) {
        NSArray *dataAux;
        
        dataAux = [dataAnalizes[i] componentsSeparatedByString:@";"];
        
        [_analizeTitle addObject:dataAux[0]];
        [_analizeText addObject:dataAux[1]];
        
        i++;
    }
    
    NSArray *dataSuspects;
    _suspects = [[NSMutableArray alloc]init];
    
    dataSuspects = [[FileManager LoadStringFromFile:suspectFilePath useBundle:true] componentsSeparatedByString:@"|"];
    
    _sizeSuspects = (int)[dataSuspects[0] integerValue];
    i = 1;
    while (i <= _sizeSuspects) {
        NSArray *dataAux;
        Suspect *suspect = [[Suspect alloc]init];
        
        dataAux = [dataSuspects[i] componentsSeparatedByString:@";"];
        
        [suspect loadDataWithArray:dataAux];
        [_suspects addObject:suspect];
        
        i++;
    }
    
}

@end