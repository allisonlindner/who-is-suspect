//
//  Pessoas.h
//  RPG
//
//  Created by Allison Lindner on 12/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person: NSObject

@property NSString *name;
@property NSString *age;
@property NSString *details;
@property NSString *state;

@end