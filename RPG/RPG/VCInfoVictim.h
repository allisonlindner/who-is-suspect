//
//  VCInfoVictim.h
//  RPG
//
//  Created by Allison Lindner on 13/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@interface VCInfoVictim : UIViewController

@property NSString *victimFileName;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelIdade;
@property (weak, nonatomic) IBOutlet UILabel *labelEstadoCivil;
@property (weak, nonatomic) IBOutlet UITextView *victimDescription;

@property Person *victim;

@end
