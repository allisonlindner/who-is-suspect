//
//  VCGameWelcome.m
//  RPG
//
//  Created by Allison Lindner on 12/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "VCGameWelcome.h"
#include "FileManager.h"
#include "AppDelegate.h"

@interface VCGameWelcome ()

@end

@implementation VCGameWelcome

-(void)viewDidLoad {
    [super viewDidLoad];
    
    //_firstRun = true;
    
    _labelCharName.text = [@"Detetive " stringByAppendingString: _characterName.capitalizedString];
    
    //Reset Button Go Mission
    [_buttonGoMission setAlpha:0.0];
    
    //Reset Label Mission Notifier
    [_labelMissionNotifier setAlpha:0.0];
    
    //Reset Text Info Hero
    [_heroInfo setAlpha:0.0];
    
    _heroInfo.text = @"";
    _heroInfo.text = [FileManager LoadStringFromFile:@"hero_01.data" useBundle:true];
    [_heroInfo scrollRangeToVisible: NSMakeRange(0, 1)];
    
    _firstRun = true;
    
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated {
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    _ready = false;
    
    if(main.control.failAcusations >= 3) {
        _heroInfo.text = @"";
        _heroInfo.text = [FileManager LoadStringFromFile:@"hero_fail.data" useBundle:true];
        [_heroInfo scrollRangeToVisible: NSMakeRange(0, 1)];
        _buttonGo.enabled = false;
        [_buttonGo setTitle:@"" forState:UIControlStateNormal];
        
        main.crimeScene.numberOfCases = 2;
        main.crimeScene.acuseResult = -2;
        main.crimeScene.currentSceneInt = 1;
        main.crimeScene.currentSceneRoom = 0;
        main.crimeScene.currentScene = @"01";
        
        main.control.successAcusations = 0;
        main.control.failAcusations = 0;
        
        _firstRun = true;
    }
    
    if(main.control.successAcusations >= main.crimeScene.numberOfCases) {
        _heroInfo.text = @"";
        _heroInfo.text = [FileManager LoadStringFromFile:@"hero_success.data" useBundle:true];
        [_heroInfo scrollRangeToVisible: NSMakeRange(0, 1)];
        _buttonGo.enabled = false;
        [_buttonGo setTitle:@"" forState:UIControlStateNormal];
        
        main.crimeScene.numberOfCases = 2;
        main.crimeScene.acuseResult = -2;
        main.crimeScene.currentSceneInt = 1;
        main.crimeScene.currentSceneRoom = 0;
        main.crimeScene.currentScene = @"01";
        
        main.control.successAcusations = 0;
        main.control.failAcusations = 0;
        
        _firstRun = true;
    }
    
    if(_firstRun)
    {
        //Animation Info Hero
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDelay:0.5];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:true];
    
        [_heroInfo setAlpha:1.0];
    
        [UIView commitAnimations];
        //Fim Animation Info Hero
        _firstRun = false;
    } else {
        _ready = true;
        [_buttonGo setTitle:@"Ler Intro" forState:UIControlStateNormal];
    }
    
    [self animateView];
    
}

- (IBAction) closeView:(id)sender {
    
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction) buttonGoAndBackPressed:(id)sender {
    
    if(_ready) {
        
        _ready = false;
        [self animateView];
        [_buttonGo setTitle:@"Avançar" forState:UIControlStateNormal];
        [_heroInfo scrollRangeToVisible: NSMakeRange(0, 1)];
        
    } else {

        _ready = true;
        [self animateView];
        [_buttonGo setTitle:@"Ler Intro" forState:UIControlStateNormal];
        
    }
    
}

- (void) animateView {
    
    if(_ready) {
        
        //Animation Label Mission Notifier
        [_labelMissionNotifier setFrame: CGRectMake(_myView.frame.origin.x+800, _labelMissionNotifier.frame.origin.y, _labelMissionNotifier.frame.size.width, _labelMissionNotifier.frame.size.height)];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:_labelMissionNotifier];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationBeginsFromCurrentState:true];
        
        [_labelMissionNotifier setAlpha:1.0];
        [_labelMissionNotifier setFrame: CGRectMake(_myView.frame.origin.x/2, _labelMissionNotifier.frame.origin.y, _labelMissionNotifier.frame.size.width, _labelMissionNotifier.frame.size.height)];
        [UIView commitAnimations];
        //Fim Animation Label Mission Notifier
        
        //Animation Button Go Mission
        [_buttonGoMission setFrame: CGRectMake(_myView.frame.origin.x-600, _buttonGoMission.frame.origin.y, _buttonGoMission.frame.size.width, _buttonGoMission.frame.size.height)];
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:_buttonGoMission];
        [UIView setAnimationDelay:0.5];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationBeginsFromCurrentState:true];
        
        [_buttonGoMission setAlpha:1.0];
        [_buttonGoMission setFrame: CGRectMake(_myView.frame.origin.x/2, _buttonGoMission.frame.origin.y, _buttonGoMission.frame.size.width, _buttonGoMission.frame.size.height)];
        
        [UIView commitAnimations];
        //Fim Animation Button Go Mission
        
        //Animation Info Hero
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:_heroInfo];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:true];
        
        [_heroInfo setAlpha:0.0];
        
        [UIView commitAnimations];
        //Fim Animation Info Hero
    } else {
        //Animation Label Mission Notifier
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:_labelMissionNotifier];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:true];
        
        [_labelMissionNotifier setAlpha:0.0];
        [UIView commitAnimations];
        //Fim Animation Label Mission Notifier
        
        //Animation Button Go Mission
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:_buttonGoMission];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:true];
        
        [_buttonGoMission setAlpha:0.0];
        [UIView commitAnimations];
        //Animation Info Hero
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:_heroInfo];
        [UIView setAnimationDelay:1.0];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:true];
        
        [_heroInfo setAlpha:1.0];
        
        [UIView commitAnimations];
        //Fim Animation Info Hero
    }
}

@end