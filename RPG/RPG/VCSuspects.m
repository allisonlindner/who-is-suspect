//
//  VCSuspects.m
//  RPG
//
//  Created by Allison Lindner on 14/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "VCSuspects.h"
#import "VCCrimeScene.h"
#import "AppDelegate.h"

@interface VCSuspects ()

@end

@implementation VCSuspects

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _currentSuspect = 0;
    [self showInformationSuspect];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeView:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void) showInformationSuspect {
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    Suspect* suspect;
 
    suspect = (Suspect*)main.crimeScene.suspects[_currentSuspect];
    
    _labelName.text = suspect.name;
    _textInterrogatorio.text = suspect.details;
    
}

- (IBAction) nextSuspect:(id)sender {
    _currentSuspect++;
    if(_currentSuspect >= _sizeSuspects){
        _currentSuspect = 0;
    }
    [self showInformationSuspect];
}

- (IBAction) backSuspect:(id)sender {
    _currentSuspect--;
    if(_currentSuspect < 0){
        _currentSuspect = _sizeSuspects - 1;
    }
    [self showInformationSuspect];
}

- (void) chooseYourDestiny {
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    Suspect* suspect;
    
    suspect = (Suspect*)main.crimeScene.suspects[_currentSuspect];
    
    if([suspect.destiny isEqualToString:@"Inocente"]) {
        
        main.crimeScene.acuseResult = _currentSuspect;
        main.control.failAcusations++;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Acusação falsa!!" message:@"Você acusou um inocente, corrija essa injustiça." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.cancelButtonIndex = 0;
        
        [alert show];
        
    } else if([suspect.destiny isEqualToString:@"Culpado"]) {
        
        main.crimeScene.acuseResult = -1;
        main.control.successAcusations++;
        
        NSString* message;
        
        message = [NSString stringWithFormat:@"O(a) %@ realmente era o(a) culpado(a). Vamos continuar e acabar com esses assassinos.", suspect.name];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ótimo trabalho!!" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.cancelButtonIndex = 1;
        
        [alert show];
        
    } else {
        main.crimeScene.acuseResult = -2;
    }
    
    main.crimeScene.currentSceneRoom = 0;
}

- (IBAction)acuse:(id)sender {
    [self chooseYourDestiny];
    
    [self dismissViewControllerAnimated:true completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end