//
//  ViewControllerCreditos.m
//  RPG
//
//  Created by Allison Lindner on 11/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "ViewControllerCreditos.h"

@implementation ViewControllerCreditos

-(IBAction)closeView:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
