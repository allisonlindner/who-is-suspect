//
//  VCSuspects.h
//  RPG
//
//  Created by Allison Lindner on 14/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Suspect.h"

@interface VCSuspects : UIViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UITextView *textInterrogatorio;

@property NSMutableArray* suspects;

@property int currentSuspect;
@property int sizeSuspects;

- (IBAction)closeView:(id)sender;
-(IBAction)acuse:(id)sender;

- (void) showInformationSuspect;
- (void) chooseYourDestiny;

@end
