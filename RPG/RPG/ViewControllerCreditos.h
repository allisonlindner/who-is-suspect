//
//  ViewControllerCreditos.h
//  RPG
//
//  Created by Allison Lindner on 11/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerCreditos : UIViewController

- (IBAction)closeView:(id)sender;

@end
