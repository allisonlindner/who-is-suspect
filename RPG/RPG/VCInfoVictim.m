//
//  VCInfoVictim.m
//  RPG
//
//  Created by Allison Lindner on 13/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "VCInfoVictim.h"
#import "FileManager.h"

@interface VCInfoVictim ()

@end

@implementation VCInfoVictim

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _labelName.text = [_labelName.text stringByAppendingString:_victim.name];
    _labelIdade.text = [_labelIdade.text stringByAppendingString:_victim.age];
    _labelEstadoCivil.text = [_labelEstadoCivil.text stringByAppendingString:_victim.state];
    [_victimDescription setText:_victim.details];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
