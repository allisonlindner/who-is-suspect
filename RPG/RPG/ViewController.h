//
//  ViewController.h
//  RPG
//
//  Created by Allison Lindner on 11/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Person.h"

@interface ViewController : UIViewController <UITextFieldDelegate> {
    CGRect labelTitleRec;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonJogar;
@property (weak, nonatomic) IBOutlet UIButton *buttonCreditos;
@property (weak, nonatomic) IBOutlet UIButton *buttonInfo;
@property (strong, nonatomic) IBOutlet UIView *myView;
@property (weak, nonatomic) IBOutlet UITextField *textNameCharacter;

- (void)animateHomeView;

@property NSArray *victim;

@end

