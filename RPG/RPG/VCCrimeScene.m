//
//  VCCrimeScene.m
//  RPG
//
//  Created by Allison Lindner on 12/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "VCCrimeScene.h"
#import "VCInfoVictim.h"
#import "VCSuspects.h"
#import "Suspect.h"
#import "FileManager.h"
#import "AppDelegate.h"

@implementation VCCrimeScene

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    //Reset Button Info Victim
    [_buttonInfoVictim setAlpha:0.0];
    
    //Reset Button Suspects
    [_buttonSuspects setAlpha:0.0];
    
    main.crimeScene.currentScene = [NSString stringWithFormat:@"%02i", main.crimeScene.currentSceneInt];
    
    [main.crimeScene LoadDataWithSceneNumber: main.crimeScene.currentScene];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    main.crimeScene.currentScene = [NSString stringWithFormat:@"%02i", main.crimeScene.currentSceneInt];
    
    if(main.control.failAcusations >= 3) {
        [self dismissViewControllerAnimated:true completion:nil];
    }
    
    [self verifyAcusation];
    [self showInformationScene];
    [self animateView];
    
    main.crimeScene.currentSceneRoom = 0;
}

- (IBAction)closeView:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void) animateView {
    
    [self showInformationScene];
    
    //Animation Button Info Victim
    [_buttonInfoVictim setFrame: CGRectMake(_myView.frame.origin.x-600, _buttonInfoVictim.frame.origin.y, _buttonInfoVictim.frame.size.width, _buttonInfoVictim.frame.size.height)];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:_buttonInfoVictim];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:true];
    
    [_buttonInfoVictim setAlpha:1.0];
    [_buttonInfoVictim setFrame: CGRectMake(_myView.frame.origin.x/2, _buttonInfoVictim.frame.origin.y, _buttonInfoVictim.frame.size.width, _buttonInfoVictim.frame.size.height)];
    [UIView commitAnimations];
    //Fim Animation Button Info Victim
    
    //Animation Button Suspects
    [_buttonSuspects setFrame: CGRectMake(_myView.frame.origin.x+600, _buttonSuspects.frame.origin.y, _buttonSuspects.frame.size.width, _buttonSuspects.frame.size.height)];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:_buttonSuspects];
    [UIView setAnimationDelay:0.5];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:true];
    
    [_buttonSuspects setAlpha:1.0];
    [_buttonSuspects setFrame: CGRectMake(_myView.frame.origin.x/2, _buttonSuspects.frame.origin.y, _buttonSuspects.frame.size.width, _buttonSuspects.frame.size.height)];
    [UIView commitAnimations];
    //Fim Animation Button Suspects
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    if([[segue identifier] isEqualToString:@"infoVictim"]) {
        
        VCInfoVictim *infoVictimVC = [segue destinationViewController];
        infoVictimVC.victim = main.crimeScene.victim;
    }
    
    if([[segue identifier] isEqualToString:@"suspectsList"]) {
        VCSuspects *suspectsVC = [segue destinationViewController];
        suspectsVC.suspects = main.crimeScene.suspects;
        suspectsVC.sizeSuspects = main.crimeScene.sizeSuspects;
    }
}

- (IBAction) nextSceneRoom:(id)sender {
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    if(main.control.success) {
        
        _buttonBackScene.hidden = false;
        _buttonInfoVictim.hidden = false;
        _buttonSuspects.hidden = false;
        
        main.control.success = false;
        
        if(main.crimeScene.currentSceneInt < main.crimeScene.numberOfCases) {
            main.crimeScene.currentSceneInt++;
        }
        
        main.crimeScene.currentScene = [NSString stringWithFormat:@"%02i", main.crimeScene.currentSceneInt];
        
        if(main.control.successAcusations >= main.crimeScene.numberOfCases) {
            [self dismissViewControllerAnimated:true completion:nil];
        }
        
        [self showInformationScene];
        
    } else {
        
        main.crimeScene.currentScene = [NSString stringWithFormat:@"%02i", main.crimeScene.currentSceneInt];
    
        main.crimeScene.currentSceneRoom++;
        if(main.crimeScene.currentSceneRoom >= main.crimeScene.sizeAnalize){
            main.crimeScene.currentSceneRoom = 0;
        }
        [self showInformationScene];
    
    }
}

- (IBAction) backSceneRoom:(id)sender {
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    main.crimeScene.currentSceneRoom--;
    if(main.crimeScene.currentSceneRoom < 0){
        main.crimeScene.currentSceneRoom = main.crimeScene.sizeAnalize - 1;
    }
    [self showInformationScene];
}

- (void) showInformationScene {
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    if(main.control.success) {
        
        _labelAnalizeTitle.text = @"Final";
        _textAnalize.text = [FileManager LoadStringFromFile: [[@"final_" stringByAppendingString:main.crimeScene.currentScene] stringByAppendingString:@".data"] useBundle:true];
        _buttonBackScene.hidden = true;
        _buttonInfoVictim.hidden = true;
        _buttonSuspects.hidden = true;
        
    } else {
    
        [main.crimeScene LoadDataWithSceneNumber: main.crimeScene.currentScene];
    
        _labelAnalizeTitle.text = [@"Analise do(a) " stringByAppendingString: main.crimeScene.analizeTitle[main.crimeScene.currentSceneRoom]];
        [_textAnalize setText:main.crimeScene.analizeText[main.crimeScene.currentSceneRoom]];
        [_textAnalize scrollRangeToVisible: NSMakeRange(0, 1)];
    
    }
}

- (void) verifyAcusation {
    
    AppDelegate *main = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
    if(main.crimeScene.acuseResult != -2) {
        
        if(main.crimeScene.acuseResult == -1) {
           
            main.crimeScene.acuseResult = -2;
            main.control.success = true;
            
            if(main.crimeScene.currentSceneInt > main.crimeScene.numberOfCases) {
                main.crimeScene.currentSceneInt = main.crimeScene.numberOfCases;
            }
            
        }
    }
}

@end
